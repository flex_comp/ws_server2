package ws_server

import (
	"fmt"
	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/protobuf2"
	"gitlab.com/flex_comp/remote_conf"
	uid "gitlab.com/flex_comp/uid2"
	"gitlab.com/flex_comp/util"
	"google.golang.org/protobuf/proto"
	"net"
	"net/http"
	"net/url"
	"sync"
	"sync/atomic"
	"time"
)

var (
	ins *WSServer
)

func init() {
	ins = &WSServer{
		clients:           make(map[interface{}]*WSClient),
		connMessage:       make(map[string]map[int64]chan *Payload),
		chConn:            make(map[int64]chan *WSClient),
		chDisconn:         make(map[int64]chan *WSClient),
		listenAddrChanged: make(map[int64]chan string),
	}

	_ = comp.RegComp(ins)
}

type FnConnVerify func(net.IP, *url.URL) (interface{}, bool)

type WSServer struct {
	open      int32
	evCounter int64

	addr             string
	headerForwardKey string
	keepAliveMS      int64

	connVerify    FnConnVerify
	connVerifyMtx sync.RWMutex

	clients    map[interface{}]*WSClient
	clientsMtx sync.RWMutex

	chConn    map[int64]chan *WSClient
	chConnMtx sync.RWMutex

	chDisconn    map[int64]chan *WSClient
	chDisconnMtx sync.RWMutex

	connMessage    map[string]map[int64]chan *Payload
	connMessageMtx sync.RWMutex

	listenAddrChanged    map[int64]chan string // [uid]监听地址变化
	listenAddrChangedMtx sync.RWMutex
}

func (w *WSServer) Init(map[string]interface{}, ...interface{}) error {
	w.headerForwardKey = util.ToString(conf.Get("websocket", "header_forward_addr"))
	w.keepAliveMS = util.ToInt64(conf.Get("websocket", "keep_alive_ms"))
	go w.listen()
	return nil
}

func (w *WSServer) Start(_ ...interface{}) error {
	return nil
}

func (w *WSServer) UnInit() {

}

func (w *WSServer) Name() string {
	return "ws-server"
}

func Open() {
	ins.Open()
}

func (w *WSServer) Open() {
	atomic.CompareAndSwapInt32(&w.open, 0, 1)
}

func Close() {
	ins.Open()
}

func (w *WSServer) Close() {
	atomic.CompareAndSwapInt32(&w.open, 1, 0)
}

func Address() string {
	return ins.Address()
}

func (w *WSServer) Address() string {
	return ins.addr
}

func OnConnVerify(fn FnConnVerify) {
	ins.OnConnVerify(fn)
}

func (w *WSServer) OnConnVerify(fn FnConnVerify) {
	w.connVerify = fn
}

func OnConnect() (chan *WSClient, int64) {
	return ins.OnConnect()
}

func (w *WSServer) OnConnect() (chan *WSClient, int64) {
	id := atomic.AddInt64(&w.evCounter, 1)
	ch := make(chan *WSClient, 1024)

	w.chConnMtx.Lock()
	defer w.chConnMtx.Unlock()

	w.chConn[id] = ch
	return ch, id
}

func OnDisconnect() (chan *WSClient, int64) {
	return ins.OnDisconnect()
}

func (w *WSServer) OnDisconnect() (chan *WSClient, int64) {
	id := atomic.AddInt64(&w.evCounter, 1)
	ch := make(chan *WSClient, 1024)

	w.chDisconnMtx.Lock()
	defer w.chDisconnMtx.Unlock()

	w.chDisconn[id] = ch
	return ch, id
}

func OnAddressChanged() (chan string, int64) {
	return ins.OnAddressChanged()
}

func (w *WSServer) OnAddressChanged() (chan string, int64) {
	id := atomic.AddInt64(&w.evCounter, 1)
	ch := make(chan string, 1024)

	w.listenAddrChangedMtx.Lock()
	defer w.listenAddrChangedMtx.Unlock()

	w.listenAddrChanged[id] = ch
	return ch, id
}

func OnMessage(head string) (chan *Payload, int64) {
	return ins.OnMessage(head)
}

func (w *WSServer) OnMessage(head string) (chan *Payload, int64) {
	id := atomic.AddInt64(&w.evCounter, 1)
	ch := make(chan *Payload, 1024)

	w.connMessageMtx.Lock()
	defer w.connMessageMtx.Unlock()

	m, ok := w.connMessage[head]
	if !ok {
		m = make(map[int64]chan *Payload)
		w.connMessage[head] = m
	}

	m[id] = ch
	return ch, id
}

func GetClient(custom interface{}) *WSClient {
	return ins.GetClient(custom)
}

func (w *WSServer) GetClient(custom interface{}) *WSClient {
	w.clientsMtx.RLock()
	defer w.clientsMtx.RUnlock()

	cli := w.clients[custom]
	return cli
}

func Broadcast(msg proto.Message) {
	ins.Broadcast(msg)
}

func (w *WSServer) Broadcast(msg proto.Message) {
	w.clientsMtx.RLock()
	defer w.clientsMtx.RUnlock()

	for _, client := range w.clients {
		client.Send(msg)
	}
}

func Send(custom interface{}, msg proto.Message) {
	ins.Send(custom, msg)
}

func (w *WSServer) Send(custom interface{}, msg proto.Message) {
	w.clientsMtx.RLock()
	defer w.clientsMtx.RUnlock()

	cli := w.clients[custom]
	if cli == nil {
		return
	}

	cli.Send(msg)
}

func (w *WSServer) listen() {
	var (
		minPort     = util.ToInt(conf.Get("websocket", "min_port"))
		maxPort     = util.ToInt(conf.Get("websocket", "max_port"))
		listenRetry = util.ToInt(conf.Get("websocket", "listen_retry"))
	)

	for {
		for port := minPort; port <= maxPort; port++ {
			addr := fmt.Sprintf(":%d", port)
			log.Info("尝试监听:", addr)

			ln, err := net.Listen("tcp", addr)
			if err != nil {
				<-time.After(time.Millisecond * time.Duration(listenRetry))
				continue
			}

			w.listenAddrChangedMtx.RLock()
			w.addr = fmt.Sprintf("%s", ln.Addr().String())
			for _, c := range w.listenAddrChanged {
				c <- w.addr
			}
			w.listenAddrChangedMtx.RUnlock()

			log.Info("监听完成:", ln.Addr().String())
			srv := http.Server{
				Addr:    ln.Addr().String(),
				Handler: http.HandlerFunc(w.onServer),
			}

			// 使用代理工具,无需手动安全监听
			//err = srv.ServeTLS(ln, cert, key)
			err = srv.Serve(ln)

			if err != nil {
				<-time.After(time.Millisecond * time.Duration(listenRetry))
				continue
			}
		}
	}
}

func (w *WSServer) onServer(rsp http.ResponseWriter, req *http.Request) {
	w.clientsMtx.Lock()
	defer w.clientsMtx.Unlock()

	var custom interface{}
	var ip net.IP

	if len(w.headerForwardKey) == 0 {
		ip = net.ParseIP(util.ParseIPByAddr(req.RemoteAddr))
	} else {
		v := req.Header.Get(w.headerForwardKey)
		if len(v) == 0 {
			// 没有IP,不可能
			rsp.WriteHeader(http.StatusUnauthorized)
			return
		}

		ip = net.ParseIP(v)
	}

	if ip == nil {
		// IP解析出错,不允许匿名
		rsp.WriteHeader(http.StatusUnauthorized)
		return
	}

	if w.connVerify != nil {
		w.connVerifyMtx.RLock()
		var ok bool
		custom, ok = w.connVerify(ip, req.URL)
		w.connVerifyMtx.RUnlock()

		if !ok {
			rsp.WriteHeader(http.StatusUnauthorized)
			return
		}
	}

	if custom == nil {
		custom = uid.Next(w.Name())
	}

	conn, _, _, err := ws.UpgradeHTTP(req, rsp)
	if err != nil {
		rsp.WriteHeader(http.StatusInternalServerError)
		log.Error("创建WS连接失败: ", err)
		return
	}

	cli := NewWSClient(custom, conn, ip)

	old := w.clients[custom]
	w.clients[custom] = cli

	if old != nil {
		old.Close()
	}

	chClose := make(chan bool, 1)
	var keepAlive *time.Timer
	if w.keepAliveMS != 0 {
		keepAlive = time.NewTimer(time.Millisecond * time.Duration(w.keepAliveMS))
	}

	go w.writePump(cli, keepAlive, chClose)
	go w.readPump(cli, keepAlive, chClose)

	w.chConnMtx.RLock()
	defer w.chConnMtx.RUnlock()

	for _, ch := range w.chConn {
		select {
		case ch <- cli:
		default:
			log.Warn("通道阻塞")
		}
	}
}

func (w *WSServer) disconnect(cli *WSClient) {
	w.clientsMtx.Lock()
	defer w.clientsMtx.Unlock()

	delete(w.clients, cli.Custom())

	w.chConnMtx.RLock()
	defer w.chConnMtx.RUnlock()

	for _, ch := range w.chDisconn {
		select {
		case ch <- cli:
		default:
			log.Warn("通道阻塞")
		}
	}
}

func (w *WSServer) readPump(cli *WSClient, heart *time.Timer, chClose chan<- bool) {
	defer func() {
		select {
		case chClose <- true:
		default:
		}
	}()

	for {
		data, op, err := wsutil.ReadClientData(cli.conn)
		if err != nil {
			log.Warn("Websocket数据读取错误, 断开连接, Address:", cli.conn.RemoteAddr(), " 错误:", err)
			return
		}

		switch op {
		case ws.OpClose:
			return
		case ws.OpText, ws.OpPong:
			continue
		case ws.OpPing:
			if heart != nil {
				heart.Reset(time.Millisecond * time.Duration(w.keepAliveMS))
			}
			continue
		}

		pkg, err := protobuf2.UnMarshal(data)
		if err != nil {
			log.Error("Websocket数据解析错误, Address:", cli.conn.RemoteAddr(), " 错误:", err)
			continue
		}

		mi := &Payload{
			Cli:     cli,
			Message: pkg,
		}

		w.connMessageMtx.RLock()
		if m, ok := w.connMessage[string(proto.MessageName(pkg))]; ok {
			for _, ch := range m {
				select {
				case ch <- mi:
				default:
					log.Warn("消息处理推送阻塞")
				}
			}
		}

		w.connMessageMtx.RUnlock()
	}
}

func (w *WSServer) writePump(cli *WSClient, heart *time.Timer, chClose <-chan bool) {
	defer func() {
		w.disconnect(cli)
		_ = cli.conn.Close()
	}()

	if heart == nil {
		for {
			select {
			case <-cli.done:
				return
			case <-chClose:
				return
			case raw := <-cli.sender:
				err := wsutil.WriteServerMessage(cli.conn, ws.OpBinary, raw)
				if err != nil {
					log.Error("发送消息失败:", err)
					return
				}
			}
		}
	} else {
		for {
			select {
			case <-cli.done:
				return
			case <-chClose:
				return
			case <-heart.C:
				return
			case raw := <-cli.sender:
				err := wsutil.WriteServerMessage(cli.conn, ws.OpBinary, raw)
				if err != nil {
					log.Error("发送消息失败:", err)
					return
				}
			}
		}
	}
}
