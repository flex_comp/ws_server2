module gitlab.com/flex_comp/ws_server2

go 1.16

require (
	github.com/gobwas/ws v1.1.0
	gitlab.com/flex_comp/comp v0.1.4
	gitlab.com/flex_comp/log v0.1.6
	gitlab.com/flex_comp/protobuf2 v0.2.1
	gitlab.com/flex_comp/remote_conf v0.1.2
	gitlab.com/flex_comp/uid2 v0.0.0-20211218203958-7fc18dd418f2
	gitlab.com/flex_comp/util v0.2.6
	google.golang.org/protobuf v1.27.1
)
