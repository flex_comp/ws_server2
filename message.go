package ws_server

import (
	"google.golang.org/protobuf/proto"
)

type Payload struct {
	Cli     *WSClient
	Message proto.Message
}
