package ws_server

import (
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/protobuf2"
	"google.golang.org/protobuf/proto"
	"net"
)

type WSClient struct {
	custom interface{}
	conn   net.Conn
	ip     net.IP
	sender chan []byte
	done   chan bool // 手动关闭
}

func NewWSClient(custom interface{}, conn net.Conn, ip net.IP) *WSClient {
	return &WSClient{
		custom: custom,
		conn:   conn,
		ip:     ip,
		sender: make(chan []byte, 1024),
		done:   make(chan bool),
	}
}

func (w *WSClient) Custom() interface{} {
	return w.custom
}

func (w *WSClient) IP() net.IP {
	return w.ip
}

func (w *WSClient) Close() {
	go func() {
		select {
		case w.done <- true:
		default:
			log.Warn("通道阻塞")
		}
	}()
}

func (w *WSClient) Send(msg proto.Message) {
	data, err := protobuf2.Marshal(msg)
	if err != nil {
		log.Error("序列化消息失败:", err)
		return
	}

	w.sender <- data
}
